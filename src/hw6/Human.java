package hw6;

import java.util.Arrays;
import java.util.Objects;

import static lib.Console.PrintF3;
import static lib.Console.printF;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private String[][] schedule;

    //поприветствовать своего любимца
    public void greetPet() {
        String animalName = family.getPet().getNickname();
        printF("Привет, %s", animalName);
    }

    //описать своего любимца
    public void describePet() {
        String isClever = family.getPet().getTrickLevel() <= 50 ? "Почти не хитрый" : "Очень хитрый";
        String animalType = String.valueOf(family.getPet().getSpecies());
        int animalAge = family.getPet().getAge();
        PrintF3("У меня есть %s!, ему %d лет, он %s", animalType, animalAge, isClever);
    }

    //конструктор, описывающий имя, фамилию и год рождения
    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    //конструктор, описывающий все поля
    public Human(String name, String surname, int year, int iq, Family family, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.family = family;
        this.schedule = schedule;
    }

    //пустой конструктор
    public Human() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human human)) return false;
        return year == human.year && iq == human.iq && name.equals(human.name) && surname.equals(human.surname) && family.equals(human.family) && Arrays.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq, family);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }
    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.println("Удаляется объект Human: " + this.toString());
        } finally {
            super.finalize();
        }
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }
}
