package hw6;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PetTest {

    @Test
    void testGetTrickLevelAnimal() {
        RoboCat pet = new RoboCat( "Peris", 13, 68, new String[]{"jump", "eat", "go for a walk", "sleeping"});
        String isClever = pet.getTrickLevel() <= 50 ? "Почти не хитрый" : "Очень хитрый";
        assertEquals("Очень хитрый", isClever);
    }

    @Test
    void testToString() {
        RoboCat pet = new RoboCat( "Peris", 13, 68, new String[]{"jump", "eat", "go for a walk", "sleeping"});
        assertEquals("Pet{" +
                "species='" + Species.DOMESTICCAT + '\'' +
                ", nickname='" + pet.getNickname() + '\'' +
                ", age=" + pet.getAge() +
                ", trickLevel=" + pet.getTrickLevel() +
                ", habits=" + Arrays.toString(pet.getHabits()) +
                '}', "Pet{" +
                "species='" + "DOMESTICCAT" + '\'' +
                ", nickname='" + "Peris" + '\'' +
                ", age=" + 13 +
                ", trickLevel=" + 68 +
                ", habits=" + "[jump"+", "+"eat"+","+ " go for a walk"+", "+ "sleeping]" +
                '}');
    }
}
