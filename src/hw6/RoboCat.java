package hw6;

import static lib.Console.print;
import static lib.Console.printF;

public class RoboCat extends Pet implements InterfaseAnimal{

    public RoboCat( String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.ROBOCAT);
    }

    @Override
    public void eat() {
        String eat = "Я кушаю!";
        print(eat);
    }
    @Override
    public void respond() {
        String animalName = getNickname();
        printF("Привет, хозяин. Я - %s. Я спал целый день и проголодался!", animalName);
    }
    @Override
    public void foul() {
        String foul = "Я разрядился хозяин и поэтому сам поключился к щитовой и сжёг её";
        print(foul);
    }
}
