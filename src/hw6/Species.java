package hw6;

public enum Species {
    DOMESTICCAT,
    DOG,
    CAT,
    FISH,
    ROBOCAT,
    UNKNOWN;

}
