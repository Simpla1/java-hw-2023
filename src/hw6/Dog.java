package hw6;

import java.util.Arrays;
import java.util.Objects;

import static lib.Console.print;
import static lib.Console.printF;

public class Dog extends Pet implements InterfaseAnimal {

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.DOG);
    }

    @Override
    public void eat() {
        String eat = "Я кушаю!";
        print(eat);
    }

    @Override
    public void respond() {
        String animalName = getNickname();
        printF("Привет, хозяин. Я - %s. Я соскучился!", animalName);
    }

    @Override
    public void foul() {
        String foul = "Нужно хорошо замести следы... после того как я побегал";
        print(foul);
    }

}
