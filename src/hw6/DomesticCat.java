package hw6;

import static lib.Console.print;
import static lib.Console.printF;

public class DomesticCat extends Pet implements InterfaseAnimal{

    public DomesticCat( String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.DOMESTICCAT);
    }

    @Override
    public void eat() {
        String eat = "Я кушаю!";
        print(eat);
    }

    @Override
    public void respond() {
        String animalName = getNickname();
        printF("Привет, хозяин. Я - %s. Я Гулял в лесу и поймал птичку", animalName);
    }
    @Override
    public void foul() {
        String foul = "Я поцарапал всю мебель, хахаха";
        print(foul);
    }

}
