package hw7;

public enum Species {
    DOMESTICCAT,
    DOG,
    CAT,
    FISH,
    ROBOCAT,
    UNKNOWN;

}
