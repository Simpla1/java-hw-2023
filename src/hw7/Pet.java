package hw7;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

public abstract class Pet {
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;
    private Species species = Species.UNKNOWN;

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public abstract void eat();

    public abstract void respond();
    public Pet(){}

    protected Pet(String nickname) {
        this.nickname = nickname;
    }

    protected Pet(String nickname, int age, int trickLevel, Set<String>habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;

    }


    protected String getNickname() {
        System.out.println("Запрос имени");
        return nickname;
    }

    protected void setNickname(String nickname) {
        this.nickname = nickname;
    }

    protected int getAge() {
        return age;
    }

    protected void setAge(int age) {
        this.age = age;
    }

    protected int getTrickLevel() {
        return trickLevel;
    }

    protected void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public String toString() {

        return "Pet{" +
                "species='" + species + '\'' +
                ", nickname='" + nickname+ '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet pet)) return false;
        return age == pet.age && trickLevel == pet.trickLevel && nickname.equals(pet.nickname) && habits.equals(pet.habits) && species == pet.species;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickname, age, trickLevel, habits, species);
    }
}
