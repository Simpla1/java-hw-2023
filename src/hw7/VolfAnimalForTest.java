package hw7;

import java.util.Set;

public class VolfAnimalForTest extends Pet {

    public VolfAnimalForTest(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }
    @Override
    public void eat() {

    }
    @Override
    public void respond() {

    }
    @Override
    public String toString() {
        return super.toString();
    }
}
