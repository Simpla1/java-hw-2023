package hw7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FamilyTest {
    Set<String> perisHabits = Set.of("jump", "eat", "go for a walk", "sleeping");
    DomesticCat Peris = new DomesticCat( "Peris", 13, 68, perisHabits);
    @org.junit.jupiter.api.Test
    void countFamily() {
        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valiev", 38);
        List<Human> children = new ArrayList<>();
        Human daughter = new Human("Katia", "Valieva", 2000);
        Human daughter2 = new Human("Olya", "Valieva", 2005);
        Human sun = new Human("Oleg", "Valiev", 1995);
        Human sun2 = new Human("Dima", "Valiev", 2010);
        Family family = new Family(mother, father, children, Peris);
        family.addChild(daughter);
        family.addChild(daughter2);
        family.addChild(sun);
        family.addChild(sun2);
        System.out.println("Количесто членов  до добавления -" + Family.countFamily);
        Human daughter3 = new Human("Tina", "Valieva", 2015);
        family.addChild(daughter3);
        System.out.println("Количесто членов после добавления -" + Family.countFamily);
        if (Family.countFamily == 19) {
            assertEquals(19, Family.countFamily);
            System.out.println("общее количество человек с тестируемыми детьми - " + Family.countFamily );
            return;
        }
        assertEquals(7, Family.countFamily);

    }

    @org.junit.jupiter.api.Test
    void addChild() {
        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valiev", 38);
        List<Human> children = new ArrayList<>();
        Human daughter = new Human("Katia", "Valieva", 2000);
        Human daughter2 = new Human("Olya", "Valieva", 2005);
        Human sun = new Human("Oleg", "Valiev", 1995);
        Human sun2 = new Human("Dima", "Valiev", 2010);

        Family family = new Family(mother, father, children, Peris);
        family.addChild(daughter);
        family.addChild(daughter2);
        family.addChild(sun);
        System.out.println("Количество детей- " +family.getChildren().size() + " до добавления -" + " " +sun2.getName());
        family.addChild(sun2);
        System.out.println("Количество детей- " +family.getChildren().size() + " после добавления -" + " " +sun2.getName());
        for (int i = 0; i < family.getChildren().size(); i++) {
            assertEquals(sun2, family.getChildren().get(3));
        }
    }

    @org.junit.jupiter.api.Test
    void deleteChild() {
        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valiev", 38);
        List<Human> children = new ArrayList<>();
        Human daughter = new Human("Katia", "Valieva", 2000);
        Human daughter2 = new Human("Olya", "Valieva", 2005);
        Human sun = new Human("Oleg", "Valiev", 1995);
        Human sun2 = new Human("Dima", "Valiev", 2010);

        Family family = new Family(mother, father, children, Peris);
        family.addChild(daughter);
        family.addChild(daughter2);
        family.addChild(sun);
        family.addChild(sun2);

        int index=1;
        children.remove(1);

         assertEquals(3, family.getChildren().size());
        System.out.println("Ребёнок удалился");
        assertEquals(3, family.getChildren().size());
        System.out.println("true");

    }

    @org.junit.jupiter.api.Test
    void deleteChildNonChanged() {
        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valiev", 38);
        List<Human> children = new ArrayList<>();
        Human daughter = new Human("Katia", "Valieva", 2000);
        Human daughter2 = new Human("Olya", "Valieva", 2005);
        Human sun = new Human("Oleg", "Valiev", 1995);
        Human sun2 = new Human("Dima", "Valiev", 2010);

        Family family = new Family(mother, father, children, Peris);
        family.addChild(daughter);
        family.addChild(daughter2);
        family.addChild(sun);
        family.addChild(sun2);
        int index=5;
        System.out.println("Переданный индекс - " + index );
        System.out.println("Количество детей до удаления - " + family.getChildren().size() );
        if (index > family.getChildren().size()) {
            System.out.println("Переданный индекс больше чем количество детей");
            System.out.println("Количество детей без изменений = " +family.getChildren().size() );
            System.out.println("false");
            return;
        }
        assertEquals(3, family.getChildren().size());
        System.out.println("Количество детей после удаления - " + family.getChildren().size() );
        System.out.println("Ребёнок удалился");
        assertEquals(3, family.getChildren().size());
        System.out.println("true");

    }


    @org.junit.jupiter.api.Test
    void testToString() {
        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valiev", 38);
        Human[] children = new Human[0];

        assertEquals("Family{" +
                "mother=" + "Alsu" + " " + "Valieva" +
                ", father=" + "Egor" + "Valiev" +
                ", children=" + Arrays.toString(children) +
                ", pet=" + "Peris" +
                '}', "Family{" +
                "mother=" + mother.getName() + " " + mother.getSurname() +
                ", father=" + father.getName() + father.getSurname() +
                ", children=" + Arrays.toString(children) +
                ", pet=" + Peris.getNickname() +
                '}');
    }
}