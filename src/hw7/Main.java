package hw7;


import java.util.*;

import static lib.Console.print;

public class Main {
    public static void start(InterfaseAnimal interfaseAnimal) {
        interfaseAnimal.foul();
    }

    public static void main(String[] args) {
        print("HW-7");

        String sunday = DayOfWeek.SUNDAY.name();
        String monday = DayOfWeek.MONDAY.name();
        String tuesday = DayOfWeek.TUESDAY.name();
        String wednesday = DayOfWeek.WEDNESDAY.name();
        String thursday = DayOfWeek.THURSDAY.name();
        String friday = DayOfWeek.FRIDAY.name();
        String saturday = DayOfWeek.SATURDAY.name();

        Map<String, String> scheduleFather= new TreeMap<>();
        scheduleFather.put(monday, "Meeting with clients");
        scheduleFather.put(tuesday, "Business lunch");
        scheduleFather.put(wednesday, "Product development meeting");
        scheduleFather.put(thursday, "Project status update");
        scheduleFather.put(friday, "Networking event");

        Map<String, String> scheduleMather= new TreeMap<>();
        scheduleMather.put(monday, "Gym");
        scheduleMather.put(tuesday, "Beauty salon");
        scheduleMather.put(wednesday, "Yoga class");
        scheduleMather.put(thursday, "Fitness club");
        scheduleMather.put(friday, "Spa day");

        Map<String, String> scheduleKatia= new HashMap<>();
        scheduleKatia.put(monday, "Rest");
        scheduleKatia.put(tuesday, "Swimming");
        scheduleKatia.put(wednesday, "Weightlifting");
        scheduleKatia.put(thursday, "Rest");
        scheduleKatia.put(friday, "Cardio");
        scheduleKatia.put(saturday, "Rest");
        scheduleKatia.put(sunday, "Yoga");

        Set<String> perisHabits = Set.of("jump", "eat", "go for a walk", "sleeping");

        RoboCat Peris = new RoboCat("Peris", 13, 68, perisHabits);
        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valieva", 38);
        Human daughter = new Human("Katia", "Valieva", 2000);
        Human daughter2 = new Human("Olya", "Valieva", 2005);
        Human sun = new Human("Oleg", "Valiev", 1995);
        Human sun2 = new Human("Dima", "Valiev", 2010);

        mother.setSchedule(scheduleMather);
        father.setSchedule(scheduleFather);
        daughter.setSchedule(scheduleKatia);


        List<Human> children = new ArrayList<>();
        Family family = new Family(mother, father, children, Peris);
        family.addChild(daughter);
        family.addChild(daughter2);
        family.addChild(sun);
        family.addChild(sun2);
        print(String.valueOf(family));

    }
}

