package hw7;

import java.util.Arrays;
import java.util.List;

import static lib.Console.print;
import static lib.Console.printF;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Pet pet;
    static int countFamily = 2;

    public static void countFamily() {
        System.out.println("Количество человек в семье: " + countFamily);
    }

    public void addChild(Human child) {
        countFamily++;
          children.add(child);
        print("Adding children successful: " + child.getName());

    }


    public boolean deleteChild(int index) {
        countFamily--;
        children.remove(index);
        System.out.println(true);
        return true;
    }



    public Family(Human mother, Human father, List<Human> children, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }
    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.println("Удаляется объект Family: " + this.toString());
        } finally {
            super.finalize();
        }
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother.getName() + " " + mother.getSurname() +
                ", father=" + father.getName() + father.getSurname() +
                ", children=" + children +
                ", pet=" + pet.getNickname() +
                '}';
    }
}
