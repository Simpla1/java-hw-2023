package lib;

import java.util.Scanner;

public class ScannerConstrained {
    private Scanner scanner = new Scanner(System.in);

    public String nextLine() {
        if (scanner.hasNextInt()) {
            throw new IllegalArgumentException("Имя не может быть числом");
        }
        return scanner.nextLine();
    }

    public String nextWeek() {
        while (scanner.hasNextInt()) {
            System.out.println("Error: a number was entered instead of a string. Please try again.");
            scanner.nextLine();
        }
        return scanner.nextLine();
    }



    public int nexInt() {
        return scanner.nextInt();
    }
}

