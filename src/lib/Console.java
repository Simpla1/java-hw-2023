package lib;

import java.util.Arrays;

public class Console {
    public static void print(String line) {
        System.out.println(line);
    }

    public static void printArr(int[] arr) {
        System.out.printf("Your numbers: %s ", Arrays.toString(arr));
    }

    public static void printArr2(String [][] arr){
        System.out.println("+-----------+-------------------------------+");
        System.out.println("|   Day     |           Activity            |");
        System.out.println("+-----------+-------------------------------+");
        for (int i = 0; i < arr.length; i++) {
            System.out.printf("| %-9s | %-29s |\n", arr[i][0], arr[i][1]);
        }
        System.out.println("+----------+--------------------------------+");
    }

    public static void printF(String st, String task) {
        System.out.printf(st, task);
        System.out.println();
      }
    public static void PrintF3(String format, Object... args) {
        System.out.printf(format, args);print("");
    }

    }

