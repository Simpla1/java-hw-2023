package hw1;



import lib.ScannerConstrained;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import static lib.Console.print;
import static lib.Console.printArr;

@SuppressWarnings("ALL")
public class main {
    public static void main(String[] args) {
        // Создаем объект класса Random Print ScannerConstrained
        Random random = new Random();
        ScannerConstrained sc = new ScannerConstrained();

        // Генерируем случайное число в диапазоне от 0 до 100
        int randomNumber = random.nextInt(101);
        int[] numsArr = new int[10];
        int index = 0;
        boolean isGuessed = false;


        //Приветствие и знакомство
        print("Please enter your name");
        String name = sc.nextLine();

        //Начало игры
        print("Let the game begin! Please enter some number you have 10 attempt");
        Scanner input = new Scanner(System.in);
        int number = 0;

        while (true) {
            System.out.print("Введите число: ");
            if (input.hasNextDouble()) {
                number = input.nextInt();
                System.out.println("Вы ввели число: " + number);
                System.err.println(String.format("загаданное число: %s;", randomNumber));
                String win = String.format("Congratulations, %s ", name);
                String smallNum = "Your number is too small. Please, try again.";
                String bigNum = "Your number is too big. Please, try again.";
                String res1 = (randomNumber == number) ? win : "";
                String res2 = (randomNumber > number) ? smallNum : "";
                String res3 = (randomNumber < number) ? bigNum : "";

                //Наполняем массив введёнными числами
                numsArr[index] = number;
                index++;
                Arrays.sort(numsArr);

                // Сортируем массив в обратном порядке
                int[] reversed = new int[numsArr.length];
                for (int i = 0; i < numsArr.length; i++) {
                    reversed[i] = numsArr[numsArr.length - 1 - i];
                }

                // Добавляем в массив строки
                String[] strings = {res1, res2, res3};
                for (String s : strings) {
                    if (s.contains(name)) {
                        isGuessed = true;
                        print(s);
                        printArr(reversed);
                        return;
                    }
                    print(s);
                }

                if (index == 10) {
                    System.err.println("Вы проиграли! начните заново (");
                }

            } else {
                System.out.println("Ошибка! Необходимо ввести число.");
                input.next();
            }

        }
    }
}
