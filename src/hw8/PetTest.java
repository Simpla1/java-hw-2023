package hw8;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PetTest {
    Set<String> perisHabits = Set.of("jump", "eat", "go for a walk", "sleeping");
    RoboCat pet = new RoboCat( "Peris", 13, 68, perisHabits);

    @Test
    void testGetTrickLevelAnimal() {
        String isClever = pet.getTrickLevel() <= 50 ? "Почти не хитрый" : "Очень хитрый";
        assertEquals("Очень хитрый", isClever);
    }

    @Test
    void testToString() {
        assertEquals("Pet{" +
                "species='" + Species.DOMESTICCAT + '\'' +
                ", nickname='" + pet.getNickname() + '\'' +
                ", age=" + pet.getAge() +
                ", trickLevel=" + pet.getTrickLevel() +
                ", habits=" + pet.getHabits() +
                '}', "Pet{" +
                "species='" + "DOMESTICCAT" + '\'' +
                ", nickname='" + "Peris" + '\'' +
                ", age=" + 13 +
                ", trickLevel=" + 68 +
                ", habits=" + "[jump"+", "+"eat"+","+ " go for a walk"+", "+ "sleeping]" +
                '}');
    }
}
