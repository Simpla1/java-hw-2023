package hw8;

import hw8.controller.FamilyController;
import hw8.dao.CollectionFamily;
import hw8.service.FamilyService;

import java.util.*;

import static lib.Console.print;

public class Main {
    public static void start(InterfaseAnimal interfaseAnimal) {
        interfaseAnimal.foul();
    }

    public static void main(String[] args) {
        print("HW-8");
        String sunday = hw7.DayOfWeek.SUNDAY.name();
        String monday = hw7.DayOfWeek.MONDAY.name();
        String tuesday = hw7.DayOfWeek.TUESDAY.name();
        String wednesday = hw7.DayOfWeek.WEDNESDAY.name();
        String thursday = hw7.DayOfWeek.THURSDAY.name();
        String friday = hw7.DayOfWeek.FRIDAY.name();
        String saturday = DayOfWeek.SATURDAY.name();

        Map<String, String> scheduleFather = new TreeMap<>();
        scheduleFather.put(monday, "Meeting with clients");
        scheduleFather.put(tuesday, "Business lunch");
        scheduleFather.put(wednesday, "Product development meeting");
        scheduleFather.put(thursday, "Project status update");
        scheduleFather.put(friday, "Networking event");

        Map<String, String> scheduleMather = new TreeMap<>();
        scheduleMather.put(monday, "Gym");
        scheduleMather.put(tuesday, "Beauty salon");
        scheduleMather.put(wednesday, "Yoga class");
        scheduleMather.put(thursday, "Fitness club");
        scheduleMather.put(friday, "Spa day");

        Map<String, String> scheduleKatia = new HashMap<>();
        scheduleKatia.put(monday, "Rest");
        scheduleKatia.put(tuesday, "Swimming");
        scheduleKatia.put(wednesday, "Weightlifting");
        scheduleKatia.put(thursday, "Rest");
        scheduleKatia.put(friday, "Cardio");
        scheduleKatia.put(saturday, "Rest");
        scheduleKatia.put(sunday, "Yoga");

        Set<String> perisHabits = Set.of("jump", "eat", "go for a walk", "sleeping");

        Set<Pet> animalValiev = new HashSet<>();


        RoboCat Peris = new RoboCat("Peris", 13, 68, perisHabits);
        animalValiev.add(Peris);

        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valieva", 38);
        Human daughter = new Human("Katia", "Valieva", 25);
        Human daughter2 = new Human("Olya", "Valieva", 18);
        Human sun = new Human("Oleg", "Valiev", 5);
        Human sun2 = new Human("Dima", "Valiev", 12);

        mother.setSchedule(scheduleMather);
        father.setSchedule(scheduleFather);
        daughter.setSchedule(scheduleKatia);

        List<Human> children = new ArrayList<>();
        Family familyValiev = new Family(mother, father, children, animalValiev);
        familyValiev.addChildren(daughter);
        familyValiev.addChildren(daughter2);
        familyValiev.addChildren(sun);
        familyValiev.addChildren(sun2);
        print(String.valueOf(familyValiev));

        //***********************************        Create family Petrov          ****
        Set<Pet> animalPetrov = new HashSet<>();
        Set<String> nemoHabits = Set.of("eat", "swiming");
        Fish fishPetrov = new Fish("Nemo", 25, 2, nemoHabits);
        animalPetrov.add(fishPetrov);
        Map<String, String> scheduleMathePetrova = new HashMap<String, String>();
        scheduleMather.put("tuesday", "Sport");
        scheduleMather.put("thursday", "work");
        scheduleMather.put("saturday", "Go for a walk");
        Map<String, String> scheduleFatherPetrov = new HashMap<String, String>();
        scheduleFather.put("monday", "Java consultation");
        scheduleFather.put("wednesday", "React consultation");
        scheduleFather.put("friday", "JavaScript consultation");

        List<Human> childrenPetrov = new ArrayList<>();
        childrenPetrov.add(daughter);
        childrenPetrov.add(daughter2);


        Human matherPetrova = new Human("Eva", "Petrova", 29, 85, scheduleMathePetrova);
        Human fatherPetrov = new Human("Vitaliy", "Korenev", 35, 99, scheduleFatherPetrov);
        Family familyPetrov = new Family(matherPetrova, fatherPetrov, childrenPetrov, animalPetrov); //дети с прошлой семьи

        //***********************************        Create family Petrov          ****
        Map<String, String> scheduleMatherSirko = new HashMap<String, String>();
        scheduleMatherSirko.put("monday", "Sport");
        scheduleMatherSirko.put("wednesday", "sleep");
        scheduleMatherSirko.put("friday", "Go for a walk");
        Map<String, String> scheduleFatherSirko = new HashMap<String, String>();
        scheduleFather.put("monday", "Java consultation");
        scheduleFather.put("wednesday", "React consultation");
        scheduleFather.put("friday", "JavaScript consultation");

        List<Human> childrenSirko = new ArrayList<>();
        childrenSirko.add(sun);
        childrenSirko.add(sun2);
        Human matherSirko = new Human("Solomiya", "Sirko", 40, 85, scheduleMathePetrova);
        Human fatherSirko = new Human("Vasil", "Sirko", 45, 99, scheduleFatherPetrov);
        Family familySirko = new Family(matherSirko, fatherSirko, childrenSirko, animalValiev); //дети с прошлой семьи

        // TODO: 26.04.2023 hw-8
        List<Family> allFamily = new ArrayList<>();
        allFamily.add(familyValiev);
        allFamily.add(familyPetrov);
        allFamily.add(familySirko);

        //Create Pet
        Set<String> dogHabits = Set.of("gaf", "eat", "go for a walk", "swimming");
        Dog Sharik = new Dog("Sharik", 12, 56, dogHabits);

        CollectionFamily collectionFamily = new CollectionFamily(allFamily);
        FamilyService familyService = new FamilyService(collectionFamily);
        FamilyController familyController = new FamilyController(familyService);
        familyController.displayAllFamilies();
        familyController.getFamilyById(1);
        familyController.getFamilyById(5);
        familyController.deleteFamily(1);
        familyController.displayAllFamilies();
        familyController.deleteFamily(familySirko);
        familyController.displayAllFamilies();
        familyController.saveFamily(familyPetrov);
        familyController.saveFamily(familySirko);
        familyController.displayAllFamilies();

        familyController.getPets(2);
        familyController.addPet(2, Sharik);
        familyController.displayAllFamilies();
        familyController.deleteAllChildrenOlderThen(19);

        Human adopCHild = new Human("Kiril","Petrov",13,110,new HashMap<>());
        familyController.adoptChild(familyPetrov,adopCHild);
    }
}

