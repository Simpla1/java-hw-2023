package hw8;

public enum Species {
    DOMESTICCAT,
    DOG,
    CAT,
    FISH,
    ROBOCAT,
    UNKNOWN;

}
