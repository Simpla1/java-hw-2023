package hw8.dao;

import hw8.Family;

public interface FamilyDao {
    void getAllFamilies();
    void getFamilyByIndex(int index);
    void deleteFamily(int index);
    void deleteFamily(Family family);
    void saveFamily(Family family);
}
