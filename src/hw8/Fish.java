package hw8;

import java.util.Set;

import static lib.Console.print;
import static lib.Console.printF;

public class Fish extends Pet {

    public Fish( String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.FISH);
    }

    @Override
    public void eat() {
        String eat = "Я кушаю!";
        print(eat);
    }
    @Override
    public void respond() {
        String animalName = getNickname();
        printF("Привет, хозяин. Я - %s. Я плавал в аквариуме в вместе с улитками!. Я соскучился!", animalName);
    }

}
