package hw8;

import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HumanTest {

    @Test
    void testToString() {
        String monday = DayOfWeek.MONDAY.name();
        String tuesday = DayOfWeek.TUESDAY.name();
        String wednesday = DayOfWeek.WEDNESDAY.name();
        String thursday = DayOfWeek.THURSDAY.name();
        String friday = DayOfWeek.FRIDAY.name();

        Map<String, String> scheduleMather= new TreeMap<>();
        scheduleMather.put(monday, "Gym");
        scheduleMather.put(tuesday, "Beauty salon");
        scheduleMather.put(wednesday, "Yoga class");
        scheduleMather.put(thursday, "Fitness club");
        scheduleMather.put(friday, "Spa day");

        Human mother = new Human("Alsu", "Valieva", 35);
        mother.setSchedule(scheduleMather);


        assertEquals("Human{" +
                "name='" + "Alsu" + '\'' +
                ", surname='" + "Valieva" + '\'' +
                ", year=" + 35 +
                '}',"Human{" +
                "name='" + mother.getName() + '\'' +
                ", surname='" + mother.getSurname() + '\'' +
                ", year=" + mother.getYear() +
                '}');

    }
}