package hw8;

import java.util.*;

public class Family {
    private Human mother;
    private Human father;
    private Human[] childrens = new Human[0];
    private Set<Pet> pet = new HashSet<>();
    public static int countFamily = 0;
    static int countHum = 0;

    public Family() {}

    public static void countFamily() {
        System.out.println("Количество человек в семье: " + countFamily);
    }

    public void addChildren(Human children) {
        Human[] humans = Arrays.copyOf(childrens, childrens.length + 1);
        children.setFamily(this);
        humans[childrens.length] = children;
        childrens = humans;

    }

    public Human[] getChildrens() {
        return childrens;
    }

    public Family(Human mother, Human father) {
        countFamily++;
        this.mother = mother;
        this.father = father;
    }
    public Family(Human mother, Human father, List<Human> childrens) {
        this.mother = mother;
        this.father = father;
        this.childrens = childrens.toArray(new Human[0]);
    }


    public void deleteChild (int index){
        if (index < 0 || index >= childrens.length) {
            return;
        }
        Human[] humans = new Human[childrens.length-1];
        System.arraycopy(childrens, 0, humans, 0, index);
        System.arraycopy(childrens, index + 1, humans, index, childrens.length - index-1);
        childrens = humans;
    }



    public Family(Human mother, Human father, List<Human> childrens, Set<Pet> pet) {
        countFamily++;
        this.mother = mother;
        this.father = father;
        this.childrens = childrens.toArray(new Human[0]);
        this.pet = pet;
    }
    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }


    public void setChildrens(Human[] childrens) {
        this.childrens = childrens;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet>pet) {
        this.pet = pet;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.println("Удаляется объект Family: " + this.toString());
        } finally {
            super.finalize();
        }
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother.getName() + " " + mother.getSurname() +
                ", father=" + father.getName() + father.getSurname() +
                ", children=" + Arrays.toString(childrens) +
                ", pet=" + pet +
                '}';
    }

}
