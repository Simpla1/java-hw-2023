package hw4;

import static lib.Console.print;

public class Main {
    public static void main(String[] args) {
        print("HW-4");
        String[][] scheduleFather = {
                {"Monday", "Meeting with clients"},
                {"Tuesday", "Business lunch"},
                {"Wednesday", "Product development meeting"},
                {"Thursday", "Project status update"},
                {"Friday", "Networking event"}
        };
        String[][] scheduleMather = {
                {"Monday", "Gym"},
                {"Tuesday", "Beauty salon"},
                {"Wednesday", "Yoga class"},
                {"Thursday", "Fitness club"},
                {"Friday", "Spa day"}
        };

        String[][] scheduleKatia = {
                {"Monday", "Rest"},
                {"Tuesday", "Swimming"},
                {"Wednesday", "Weightlifting"},
                {"Thursday", "Rest"},
                {"Friday", "Cardio"},
                {"Saturday", "Rest"},
                {"Sunday", "Yoga"}
        };

        Pet Peris = new Pet("Cat", "Peris", 13, 68, new String[]{"jump", "eat", "go for a walk", "sleeping"});
        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valieva", 38);
        Human daughter = new Human("Katia", "Valieva", 2000);
        Human daughter2 = new Human("Olya", "Valieva", 2005);
        Human sun = new Human("Oleg", "Valiev", 1995);
        Human sun2 = new Human("Dima", "Valiev", 2010);

        mother.setSchedule(scheduleMather);
        father.setSchedule(scheduleFather);
        daughter.setSchedule(scheduleKatia);


        Human[] children = new Human[0];
        Family family = new Family(mother, father, children, Peris);


        Peris.eat();
        Peris.respond();
        Peris.foul();
        print(Peris.toString());
        print(family.toString());
        print(mother.toString());
        print(father.toString());
        print(daughter.toString());

        family.addChild(daughter);
        family.addChild(daughter2);
        family.addChild(sun);
        family.addChild(sun2);
        print("Узнаем про кошку");

        ;
        print(family.toString());
        System.err.println("Детей в семье перед удалением: " + String.valueOf(family.getChildren().length));
        Family.countFamily();

        family.deleteChild(1);
        System.err.println("Детей в семье после удаления: " + String.valueOf(family.getChildren().length));
        print("***");
        print(family.toString());
        Family.countFamily();
        System.out.println("приветствие животного");
        daughter.greetPet();
        daughter.describePet();
    }
}
