package hw4;

import java.util.Arrays;

import static lib.Console.PrintF3;
import static lib.Console.printF;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;
    static int countFamily = 2;

    public static void countFamily() {
        System.out.println("Количество человек в семье: " + countFamily);
    }

    public void addChild(Human child) {
        countFamily++;
        Human[] humans = Arrays.copyOf(children, children.length + 1);
        child.setFamily(this);
        humans[children.length] = child;
        children = humans;
    }

    public boolean deleteChild(int index) {
        countFamily--;
        if (index < 0 || index >= children.length) {
            return false;
        }
        Human[] humans = new Human[children.length - 1];
        System.arraycopy(children, 0, humans, 0, index);
        System.arraycopy(children, index + 1, humans, index, children.length - index - 1);
        children = humans;
        System.out.println(true);
        return true;
    }


    public Family(Human mother, Human father, Human[] children, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother.getName() + " " + mother.getSurname() +
                ", father=" + father.getName() + father.getSurname() +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet.getNickname() +
                '}';
    }
}
