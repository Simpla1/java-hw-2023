package hw2;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

import static hw2.InitCanvas.initCanvas;
import static hw2.ShowCanvas.showCanvas;
import static lib.Console.print;

public class Main {

    public static void main(String[] args) {
        Random randFirst = new Random();
        Random randTwo = new Random();
        int a = 0;
        int b = 4;
        int x  =  a + randFirst.nextInt(b - a + 1);
        int y  =  a + randTwo.nextInt(b - a + 1);
        Scanner in = new Scanner(System.in);
        System.out.print("Input name: ");
        String name = in.nextLine();
        print("Hello my friend " + name);
        String[][] canvas = initCanvas();
        while (true) {
            print("Let the game begin! Please enter");
            showCanvas(canvas);
            String numXGet = in.nextLine();
            String numYGet = in.nextLine();
            try {
                int numX = Integer.parseInt(numXGet);
                int numY = Integer.parseInt(numYGet);
                if (numX > 5 || numX < 1 || numY > 5 || numY < 1) {
                    print("You must enter a value between 1-5: ");
                } else {
                    canvas[numX-1][numY-1] ="*";
                    canvas[x][y] = "0";
                    if (Objects.equals(canvas[x][y], canvas[numX-1][numY-1])) {
                        print(name + " You have won!");
                        canvas[x][y] = "Х";
                        showCanvas(canvas);
                        break;
                    }
                }
            } catch (NumberFormatException e) {
                System.err.println("\n" + "Wrong string format!" + e);
                print(name + " please, try again only numbers.");
            }
        }
    }

}

