package hw2;

public class ShowCanvas {
    public static void showCanvas(String[][] canvas) {
        System.out.print("0 | ");
        for (int i = 0; i < canvas.length; i++) {
            System.out.print(i + 1 + " | ");
        }
        System.out.println();
        for (int i = 0; i < canvas.length; i++) {
            System.out.print(i + 1 + " | ");
            for (int j = 0; j < canvas[i].length; j++) {
                System.out.printf("%s | ", canvas[i][j]);
            }
            System.out.println();
        }
    }
}
