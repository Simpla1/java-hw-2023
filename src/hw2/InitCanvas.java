package hw2;

import java.util.Arrays;

public class InitCanvas {
    public static String[][] initCanvas() {
        String[][] canvas = new String[5][5];
        for (String[] strings : canvas) {
            Arrays.fill(strings, "-");
        }
        return canvas;
    }
}
