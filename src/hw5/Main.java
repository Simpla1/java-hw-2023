package hw5;


import static lib.Console.print;

public class Main {
    public static void main(String[] args) {
        print("HW-5");

        //Создание в классе в цикле объектов типа Human тобы сборщик мусора начал удалять
        // старые объекты (от 10 тысяч до 10 миллионов, в зависимости от объема доступной оперативной памяти).

        //****** ДЛЯ АКТИВАЦИИ НЕОБХОДИМО РАЗКОММЕНТИРОВАТЬ КОД НИЖЕ
//        int num=0;
//        int range = 10000000;
//        for (int i = 0; i <range ; i++) {
//            Human human = new Human();
//            num++;
//            System.out.println("Ссоздался объект Human №: " + num);
//        }


        //Создание enum DayOfWeek со списком дней недели

        String sunday = DayOfWeek.SUNDAY.name();
        String monday = DayOfWeek.MONDAY.name();
        String tuesday = DayOfWeek.TUESDAY.name();
        String wednesday = DayOfWeek.WEDNESDAY.name();
        String thursday = DayOfWeek.THURSDAY.name();
        String friday = DayOfWeek.FRIDAY.name();
        String saturday = DayOfWeek.SATURDAY.name();

        String[][] scheduleFather = {
                {monday, "Meeting with clients"},
                {thursday, "Business lunch"},
                {wednesday, "Product development meeting"},
                {thursday, "Project status update"},
                {friday, "Networking event"}
        };
        String[][] scheduleMather = {
                {monday, "Gym"},
                {tuesday, "Beauty salon"},
                {wednesday, "Yoga class"},
                {thursday, "Fitness club"},
                {friday, "Spa day"}
        };

        String[][] scheduleKatia = {
                {monday, "Rest"},
                {tuesday, "Swimming"},
                {wednesday, "Weightlifting"},
                {thursday, "Rest"},
                {friday, "Cardio"},
                {saturday, "Rest"},
                {sunday, "Yoga"}
        };

        Pet Peris = new Pet(Species.CAT, "Peris", 13, 68, new String[]{"jump", "eat", "go for a walk", "sleeping"});
        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valieva", 38);
        Human daughter = new Human("Katia", "Valieva", 2000);
        Human daughter2 = new Human("Olya", "Valieva", 2005);
        Human sun = new Human("Oleg", "Valiev", 1995);
        Human sun2 = new Human("Dima", "Valiev", 2010);

        mother.setSchedule(scheduleMather);
        father.setSchedule(scheduleFather);
        daughter.setSchedule(scheduleKatia);


        Human[] children = new Human[0];
        Family family = new Family(mother, father, children, Peris);


        Peris.eat();
        Peris.respond();
        Peris.foul();
        print(Peris.toString());
        print(family.toString());
        print(mother.toString());
        print(father.toString());
        print(daughter.toString());

        family.addChild(daughter);
        family.addChild(daughter2);
        family.addChild(sun);
        family.addChild(sun2);
        print("Узнаем про кошку");
        daughter.greetPet();
        daughter.describePet();
        print(family.toString());
        System.err.println("Детей в семье перед удалением: " + String.valueOf(family.getChildren().length));
        Family.countFamily();

        family.deleteChild(1);
        System.err.println("Детей в семье после удаления: " + String.valueOf(family.getChildren().length));
        print("***");
        print(family.toString());
        Family.countFamily();
    }
 }

