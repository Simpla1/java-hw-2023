package hw5;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FamilyTest {

    @org.junit.jupiter.api.Test
    void countFamily() {
        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valiev", 38);
        Human[] children = new Human[0];
        Human daughter = new Human("Katia", "Valieva", 2000);
        Human daughter2 = new Human("Olya", "Valieva", 2005);
        Human sun = new Human("Oleg", "Valiev", 1995);
        Human sun2 = new Human("Dima", "Valiev", 2010);
        Pet Peris = new Pet(Species.CAT, "Peris", 13, 68, new String[]{"jump", "eat", "go for a walk", "sleeping"});

        Family family = new Family(mother, father, children, Peris);
        family.addChild(daughter);
        family.addChild(daughter2);
        family.addChild(sun);
        family.addChild(sun2);
        System.out.println("Количесто членов  до добавления -" + Family.countFamily);
        Human daughter3 = new Human("Tina", "Valieva", 2015);
        family.addChild(daughter3);
        System.out.println("Количесто членов после добавления -" + Family.countFamily);
        if (Family.countFamily == 19) {
            assertEquals(19, Family.countFamily);
            System.out.println("общее количество человек с тестируемыми детьми - " + Family.countFamily );
            return;
        }
        assertEquals(7, Family.countFamily);

    }

    @org.junit.jupiter.api.Test
    void addChild() {
        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valiev", 38);
        Human[] children = new Human[0];
        Human daughter = new Human("Katia", "Valieva", 2000);
        Human daughter2 = new Human("Olya", "Valieva", 2005);
        Human sun = new Human("Oleg", "Valiev", 1995);
        Human sun2 = new Human("Dima", "Valiev", 2010);
        Pet Peris = new Pet(Species.CAT, "Peris", 13, 68, new String[]{"jump", "eat", "go for a walk", "sleeping"});

        Family family = new Family(mother, father, children, Peris);
        family.addChild(daughter);
        family.addChild(daughter2);
        family.addChild(sun);
        System.out.println("Количество детей- " +family.getChildren().length + " до добавления -" + " " +sun2.getName());
        family.addChild(sun2);
        System.out.println("Количество детей- " +family.getChildren().length + " после добавления -" + " " +sun2.getName());
        for (int i = 0; i < family.getChildren().length ; i++) {
            assertEquals(sun2,family.getChildren()[3]);
        }
    }

    @org.junit.jupiter.api.Test
    void deleteChild() {
        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valiev", 38);
        Human[] children = new Human[0];
        Human daughter = new Human("Katia", "Valieva", 2000);
        Human daughter2 = new Human("Olya", "Valieva", 2005);
        Human sun = new Human("Oleg", "Valiev", 1995);
        Human sun2 = new Human("Dima", "Valiev", 2010);
        Pet Peris = new Pet(Species.CAT, "Peris", 13, 68, new String[]{"jump", "eat", "go for a walk", "sleeping"});

        Family family = new Family(mother, father, children, Peris);
        family.addChild(daughter);
        family.addChild(daughter2);
        family.addChild(sun);
        family.addChild(sun2);

        Human[] humans = new Human[family.getChildren().length - 1];
        int index=1;
        System.arraycopy(family.getChildren(), 0, humans, 0, index);
        System.arraycopy(family.getChildren(), index + 1, humans, index, family.getChildren().length - index - 1);
        family.setChildren(humans);
         assertEquals(3, family.getChildren().length);
        System.out.println("Ребёнок удалился");
        assertTrue(family.getChildren().length == 3);
        System.out.println("true");

    }

    @org.junit.jupiter.api.Test
    void deleteChildNonChanged() {
        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valiev", 38);
        Human[] children = new Human[0];
        Human daughter = new Human("Katia", "Valieva", 2000);
        Human daughter2 = new Human("Olya", "Valieva", 2005);
        Human sun = new Human("Oleg", "Valiev", 1995);
        Human sun2 = new Human("Dima", "Valiev", 2010);
        Pet Peris = new Pet(Species.CAT, "Peris", 13, 68, new String[]{"jump", "eat", "go for a walk", "sleeping"});

        Family family = new Family(mother, father, children, Peris);
        family.addChild(daughter);
        family.addChild(daughter2);
        family.addChild(sun);
        family.addChild(sun2);
        System.out.println("1 - " +family.getChildren().length);
        Human[] humans = new Human[family.getChildren().length - 1];
        int index=5;
        System.out.println("Переданный индекс - " + index );
        System.out.println("Количество детей до удаления - " + family.getChildren().length );
        if (index > family.getChildren().length) {
            System.out.println("Переданный индекс больше чем количество детей");
            System.out.println("Количество детей без изменений = " +family.getChildren().length );
            System.out.println("false");
            return;
        }
        System.arraycopy(family.getChildren(), 0, humans, 0, index);
        System.arraycopy(family.getChildren(), index + 1, humans, index, family.getChildren().length - index - 1);
        family.setChildren(humans);
        assertEquals(3, family.getChildren().length);
        System.out.println("Количество детей после удаления - " + family.getChildren().length );
        System.out.println("Ребёнок удалился");
        assertTrue(family.getChildren().length == 3);
        System.out.println("true");

    }


    @org.junit.jupiter.api.Test
    void testToString() {
        Human mother = new Human("Alsu", "Valieva", 35);
        Human father = new Human("Egor", "Valiev", 38);
        Human[] children = new Human[0];
        Pet pet = new Pet(Species.CAT, "Peris", 13, 68, new String[]{"jump", "eat", "go for a walk", "sleeping"});

        assertEquals("Family{" +
                "mother=" + "Alsu" + " " + "Valieva" +
                ", father=" + "Egor" + "Valiev" +
                ", children=" + Arrays.toString(children) +
                ", pet=" + "Peris" +
                '}', "Family{" +
                "mother=" + mother.getName() + " " + mother.getSurname() +
                ", father=" + father.getName() + father.getSurname() +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet.getNickname() +
                '}');
    }
}