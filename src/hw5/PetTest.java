package hw5;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PetTest {


    @Test
    void testGetTrickLevelAnimal() {
        Pet pet = new Pet(Species.CAT, "Peris", 13, 68, new String[]{"jump", "eat", "go for a walk", "sleeping"});
        String isClever = pet.getTrickLevel() <= 50 ? "Почти не хитрый" : "Очень хитрый";
        assertEquals("Очень хитрый", isClever);
    }

    @Test
    void testToString() {

        Pet pet = new Pet(Species.CAT, "Peris", 13, 68, new String[]{"jump", "eat", "go for a walk", "sleeping"});
        assertEquals("Pet{" +
                "species='" + pet.getSpecies() + '\'' +
                ", nickname='" + pet.getNickname() + '\'' +
                ", age=" + pet.getAge() +
                ", trickLevel=" + pet.getTrickLevel() +
                ", habits=" + Arrays.toString(pet.getHabits()) +
                '}', "Pet{" +
                "species='" + "CAT" + '\'' +
                ", nickname='" + "Peris" + '\'' +
                ", age=" + 13 +
                ", trickLevel=" + 68 +
                ", habits=" + "[jump"+", "+"eat"+","+ " go for a walk"+", "+ "sleeping]" +
                '}');
    }
}
