package hw5;

import hw4.Human;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class HumanTest {


    @Test
    void testToString() {
        String monday = DayOfWeek.MONDAY.name();
        String tuesday = DayOfWeek.TUESDAY.name();
        String wednesday = DayOfWeek.WEDNESDAY.name();
        String thursday = DayOfWeek.THURSDAY.name();
        String friday = DayOfWeek.FRIDAY.name();

        String[][] scheduleMather = {
                {monday, "Gym"},
                {tuesday, "Beauty salon"},
                {wednesday, "Yoga class"},
                {thursday, "Fitness club"},
                {friday, "Spa day"}
        };
        Human mother = new Human("Alsu", "Valieva", 35);
        mother.setSchedule(scheduleMather);


        assertEquals("Human{" +
                "name='" + "Alsu" + '\'' +
                ", surname='" + "Valieva" + '\'' +
                ", year=" + 35 +
                '}',"Human{" +
                "name='" + mother.getName() + '\'' +
                ", surname='" + mother.getSurname() + '\'' +
                ", year=" + mother.getYear() +
                '}');

    }
}