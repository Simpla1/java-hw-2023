package hw3;

import lib.ScannerConstrained;

import java.util.Objects;
import java.util.Scanner;

import static lib.Console.*;

public class Main {

    public static void main(String[] args) {

        Schedule schedule = new Schedule();
        ScannerConstrained sc = new ScannerConstrained();
        printArr2(schedule.schedule);
        while (true) {
            print("Please, input the day of the week if you can to change task for some day you should enter change:");
            String dayOfWeek = sc.nextWeek();
            String adaptiveWord = dayOfWeek.substring(0, 1).toUpperCase() + dayOfWeek.substring(1).trim();
            String change = "Change";
            String prevDay = "";
            boolean isChange = true;
            if (Objects.equals(adaptiveWord, "Exit")) return;
            if (!adaptiveWord.equals(change)) {
                for (int i = 0; i < schedule.schedule.length; i++) {
                    for (int j = 0; j < schedule.schedule[i].length; j++) {
                        String st = schedule.schedule[i][j];
                        prevDay = st;
                        switch (st.compareTo(adaptiveWord)) {
                            case 0:
                                String getTask = schedule.schedule[i][j + 1];
                                printF("Your tasks for Monday: %s", getTask);
                                break;
                        }
                    }
                }
            } else {
                schedule.changeSchedule();
            }

        }
    }
}
