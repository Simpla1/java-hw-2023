package hw3;

import lib.ScannerConstrained;

import java.util.Arrays;
import java.util.Scanner;

import static lib.Console.*;

public class Schedule {
    String[][] schedule;
    ScannerConstrained sc = new ScannerConstrained();

    public Schedule() {
        schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses;";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go for a walk with a dog";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "go to the airport";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "go to the park";
        schedule[5][0] = "Friday";
        schedule[5][1] = "go through passport control";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "go fishing";
    }
    public void changeSchedule() {
        Scanner scanner = new Scanner(System.in);
        printArr2(schedule);
        print("please enter current day for change activity");
        String dayOfWeek = sc.nextWeek();
        String adaptiveWord = dayOfWeek.substring(0, 1).toUpperCase() + dayOfWeek.substring(1).trim();

        for (int i = 0; i < schedule.length; i++) {
            if (schedule[i][0].equals(adaptiveWord)) {
                printF("New activity for: %s ", adaptiveWord);
                String newActivity = scanner.nextLine();
                schedule[i][1] = newActivity;
                System.out.println("Schedule updated:");
                printArr2(schedule);
                return;
            }
        }

    }

}